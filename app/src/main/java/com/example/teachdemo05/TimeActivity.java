package com.example.teachdemo05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

public class TimeActivity extends AppCompatActivity {

    Button btnGetTime;
    TimePicker timePicker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);
        btnGetTime = findViewById(R.id.btnGetTime);
        timePicker = findViewById(R.id.timePicker);

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hour, int minute) {
                Toast.makeText(TimeActivity.this, ""+hour +":" + minute, Toast.LENGTH_SHORT).show();
            }
        });
        btnGetTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(TimeActivity.this, ""+timePicker.getHour() +":" + timePicker.getMinute(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
