package com.example.teachdemo05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.SystemClock;
import android.widget.Button;
import android.widget.Chronometer;

public class ChronoActivity extends AppCompatActivity {
    Button btnSend;
    Chronometer chronometer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chrono);
        chronometer = findViewById(R.id.chronometer);
        btnSend = findViewById(R.id.btnSend);
        chronometer.setCountDown(true);
        chronometer.setBase(SystemClock.elapsedRealtime() + 60000);
        chronometer.start();//stop
        btnSend.setEnabled(false);

        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                btnSend.setText(chronometer.getText());
                if(chronometer.getText().equals("00:00")){
                    chronometer.stop();
                    btnSend.setText("发送验证码");
                    btnSend.setEnabled(true);
                }
            }
        });
    }
}
