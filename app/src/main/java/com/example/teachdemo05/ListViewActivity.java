package com.example.teachdemo05;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListViewActivity extends AppCompatActivity {

    ListView lv_contact;
    int[] imgIds = new int[]{
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06,
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06,
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06,
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06,
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06,
            R.mipmap.img01,R.mipmap.img02,R.mipmap.img03,
            R.mipmap.img04,R.mipmap.img05,R.mipmap.img06
    };
    String[] strNames = new String[]{
            "1111","2222","33333","44444","5555","66666",
            "1111","2222","33333","44444","5555","66666",
            "1111","2222","33333","44444","5555","66666",
            "1111","2222","33333","44444","5555","66666",
            "1111","2222","33333","44444","5555","66666",
            "1111","2222","33333","44444","5555","66666"

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        lv_contact = findViewById(R.id.lv_contact);

        List<Map<String,String>> list = new ArrayList<>();
        for(int m=0;m<imgIds.length;m++){
            Map<String,String> map = new HashMap<>();
            map.put("imgKey",String.valueOf(imgIds[m]));
            map.put("nameKey",strNames[m]);

            list.add(map);
        }


        String[] from = new String[]{"imgKey","nameKey"};
        int[] to = new int[]{R.id.iv_contact,R.id.tv_contact};
        //SimpleAdapter adapter = new SimpleAdapter(ListViewActivity.this,list,R.layout.lv_item,from,to);
        ContactSimpleAdapter adapter = new ContactSimpleAdapter(ListViewActivity.this,list,R.layout.lv_item,from,to);
        lv_contact.setAdapter(adapter);
        /*
        for(int i = 0;i<strNames.length;){


            View view = (View) lv_contact.getChildAt(i);
            TextView tv = findViewById(R.id.tv_contact);
            tv.setTextColor(Color.RED);
            i = i+6;
        }*/
        /*
        int m=1000000,c=0;
        for(int i = 0;i<m;){
            c++;
            i = i+6;
        }
        Log.v("count:",""+c);

        */

    }


    class ContactSimpleAdapter  extends SimpleAdapter{

        Context context;
        List<? extends Map<String, ?>> data;
        public ContactSimpleAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
            this.context = context;
            this.data = data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = LayoutInflater.from(context).inflate(R.layout.lv_item,parent,false);
            TextView tv_contact = convertView.findViewById(R.id.tv_contact);
            ImageView iv_contact = convertView.findViewById(R.id.iv_contact);
            iv_contact.setImageResource(imgIds[position]);
            tv_contact.setText(strNames[position]);
            if(position%6==0){
                //tv_contact.setBackgroundColor(Color.RED);
                tv_contact.setTextColor(Color.RED);
            }else {
                tv_contact.setTextColor(Color.GRAY);
            }

            return super.getView(position, convertView, parent);
        }
    }

}
