package com.example.teachdemo05;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.teachdemo05.model.User;

import java.util.ArrayList;

public class Test2Activity extends AppCompatActivity {
    TextView tv_msg;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_test2);

        tv_msg = findViewById(R.id.tv_msg);

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        tv_msg.setText(bundle.getString("str_key","哈哈，你拿不到我") + "  " + bundle.getInt("int_key",0));

        String[] strArray=bundle.getStringArray("array_key");
        for (int i=0;i<strArray.length;i++){
            Log.v("data"+i," "+strArray[i]);
        }
        int k=0;
        for(String str:strArray){
            Log.v("data"+ k++," "+str);
        }
        ArrayList<String> list=bundle.getStringArrayList("list_key");

        for (String str:list){
            Log.v("data",""+str);
        }
        User user=(User) bundle.getSerializable("user_key");
        Log.v("user:",user.toString());

    }
}
