package com.example.teachdemo05.model;

import java.io.Serializable;

public class User implements Serializable {
    private  String uid;
    private  String uname;
    public User(){}
    public User(String uid){
        this.uid=uid;
    }
    public  User(String uid,String uname){
        this.uid=uid;
        this.uname=uname;
    }
    public String getUid(){
        return uid;
    }
    public  void setUid(String uid){
        this.uid=uid;
    }
    public String getUname(){
        return uname;
    }
    public void setUname(String uname){
        this.uname=uname;
    }
    public  String toString(){
        return "用户[" +
                "用户ID='"+uid+'\''+
                ",用户名='"+uname+'\''+
                ']';
    }
}
