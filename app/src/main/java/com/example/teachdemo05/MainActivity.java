package com.example.teachdemo05;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnSubmit;
    ImageButton imgbtnGet;
    TextView tvMsg;
    EditText etPhone,etBirth;
    RadioGroup rgSex;
    RadioButton rbtnMale,rbtnFemale;
    CheckBox chkJava,chkFront,chkPython;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uidemo);

        btnSubmit = findViewById(R.id.btnSubmit);//找出java变量与视图（view-组件）之间的关联（id）

        imgbtnGet = findViewById(R.id.imgbtnGet);//找出java变量与视图（view-组件）之间的关联（id）
        tvMsg = findViewById(R.id.tvMsg);//找出java变量与视图（view-组件）之间的关联（id）
        etPhone = findViewById(R.id.etPhone);//找出java变量与视图（view-组件）之间的关联（id）
        etBirth = findViewById(R.id.etBirth);//找出java变量与视图（view-组件）之间的关联（id）

        rgSex = findViewById(R.id.rgSex);//找出java变量与视图（view-组件）之间的关联（id）
        rbtnMale = findViewById(R.id.rbtnMale);//找出java变量与视图（view-组件）之间的关联（id）
        rbtnFemale = findViewById(R.id.rbtnFemale);//找出java变量与视图（view-组件）之间的关联（id）

        chkJava = findViewById(R.id.chkJava);//找出java变量与视图（view-组件）之间的关联（id）
        chkFront = findViewById(R.id.chkFront);//找出java变量与视图（view-组件）之间的关联（id）
        chkPython = findViewById(R.id.chkPython);//找出java变量与视图（view-组件）之间的关联（id）

        rgSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                System.out.println("id:"+radioGroup.getCheckedRadioButtonId());//2131165311 --- 女  2131165312--男
                if(radioGroup.getCheckedRadioButtonId() == R.id.rbtnMale){
                    System.out.println("male");
                }
                if(radioGroup.getCheckedRadioButtonId() == R.id.rbtnFemale){
                    System.out.println("female");
                }
            }
        });

        chkJava.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                System.out.println("java");
            }
        });
        chkFront.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                System.out.println("front");
            }
        });
        chkPython.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                System.out.println("python");
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvMsg.setText("msg");
                System.out.println("msg");
                String strSex = "";
                if(rbtnMale.isChecked()){
                    strSex = " 性别：男";
                }
                if(rbtnFemale.isChecked()){
                    strSex = " 性别：女";
                }
                String strCourse=" 课程：";
                if(chkJava.isChecked()){
                    strCourse += "Java ";
                }
                if(chkFront.isChecked()){
                    strCourse += "前端 ";
                }
                if(chkPython.isChecked()){
                    strCourse += "Python ";
                }
                Toast.makeText(MainActivity.this, etPhone.getText() + strSex + strCourse, Toast.LENGTH_SHORT).show();

            }
        });

        etBirth.setEnabled(false);
        imgbtnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog pickerDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        etBirth.setText("" + year + "-" + (month + 1) + "-" + day);
                    }
                },2020,9,16);

                pickerDialog.show();
            }
        });
    }
}
