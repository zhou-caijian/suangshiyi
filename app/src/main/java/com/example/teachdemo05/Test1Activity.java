package com.example.teachdemo05;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import  com.example.teachdemo05.model.User;

public class Test1Activity extends AppCompatActivity {

    Button btn_go;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test1);

        btn_go = findViewById(R.id.btn_go);

        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Test1Activity.this,Test2Activity.class);

                String strValue = "Android";
                int intValue = 1010;

                String[] strArray=new String[]{"111","222","333"};
                List<String> list=new ArrayList<>();
                list.add("aaa");
                list.add("bbb");
                list.add("ccc");

                User user=new User("10001","zhangsan");
                User user1=new User("10002","lisi");
                User user2=new User("10003","wangwu");
                User user3=new User("10004","xiaowu");
                List<User> userList=new ArrayList<>();
                userList.add(user1);
                userList.add(user2);
                userList.add(user3);

                Bundle bundle = new Bundle();
                bundle.putString("str_key",strValue);
                bundle.putInt("int_key",intValue);
                bundle.putStringArray("array_key",strArray);
                bundle.putStringArrayList("list_key",(ArrayList<String>) list);
                bundle.putSerializable("user_key",user);

                intent.putExtras(bundle);

                startActivity(intent);//启动目标Activity

                finish();//关闭当前Activity
            }
        });
    }
}
